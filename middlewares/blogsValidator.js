const { body } = require("express-validator");

module.exports = [

    body('title')

        .notEmpty()
        
        .withMessage("Title is required"),

    body('title')

        .not()

        .isInt()
        
        .withMessage("Title must be a Text"),

    body('content')

        .notEmpty()
        
        .withMessage("Content is required"),

    body('category')

        .notEmpty()
        
        .withMessage("Category is required")
        
]