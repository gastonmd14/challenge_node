var express = require('express');
var router = express.Router();
var path = require('path')
var multer  = require('multer');

var storage = multer.diskStorage({

  destination: (req, file, cb) => {

    cb(null, 'public/images')

  },

  filename: (req, file, cb) => {

    cb(null, file.fieldname + '-' + Date.now()+ path.extname(file.originalname))

  }

})

var upload = multer({ storage: storage });

var blogsController = require('../controllers/blogsController')
var blogsValidator = require('../middlewares/blogsValidator')

/* GET ALL POSTS */
router.get('/', blogsController.index);

/* GET ONE POSTS */
router.get('/:id', blogsController.detail);

/* CREATE ONE POSTS */
router.post('/', upload.any(), blogsValidator, blogsController.create);

/* UPDATE ONE POSTS */
router.patch('/:id', blogsValidator, blogsController.update);

/* DELETE ONE POSTS */
router.delete('/:id', blogsController.destroy);

module.exports = router;