-- MySQL Script generated by MySQL Workbench
-- Tue Apr 20 16:45:35 2021
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema Blogs
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema Blogs
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `Blogs` DEFAULT CHARACTER SET utf8mb4 ;
USE `Blogs` ;

-- -----------------------------------------------------
-- Table `Blogs`.`blogs`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Blogs`.`blogs` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(200) NULL,
  `content` LONGTEXT NULL,
  `image` VARCHAR(255) NULL,
  `category` VARCHAR(100) NULL,
  `createdAt` DATETIME NULL,
  `updatedAt` DATETIME NULL,
  `deletedAt` DATETIME NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
