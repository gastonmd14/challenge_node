module.exports = (sequelize, datatypes) => {
    let alias = "Blog"
    let cols = {

        id: {
            type: datatypes.INTEGER.UNSIGNED,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false
        },

        title: {
            type: datatypes.STRING
        },

        content: {
            type: datatypes.TEXT('long')

        },

        image: {
            type: datatypes.STRING

        },

        category: {
            type: datatypes.STRING

        },

        createdAt: {
            type: datatypes.DATE

        },

        updatedAt: {
            type: datatypes.DATE

        },

        deletedAt: {
            type: datatypes.DATE

        }

    }

    let config = {
        tableName: "blogs",
        timestamps: true,
        paranoid: true,

    }

    let Blog = sequelize.define(alias, cols, config);

    Blog.associate = function(models) { 
       
    };

    
    return Blog;

};