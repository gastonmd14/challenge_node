# Alkemy - Challenge Node #

Este challenge de Node es una API para un blog, utilizando Node y Express.



### Herramientas Utilizadas ###

- [NodeJS - Motor de JavaScript V8 de Chrome](https://nodejs.org/es/)
- [Experss - Infraestructura web rápida, minimalista y flexible para Node.js](https://expressjs.com/es/)
- [MySQL - Servicio de Base de Datos](https://www.mysql.com/)



### Realizado por: ###

* Dominguez Gaston Matias
