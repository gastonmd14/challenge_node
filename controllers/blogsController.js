const { validationResult } = require('express-validator');
const db = require('../db/models');

module.exports = {

index: (req, res, next) => {

  db.Blog.findAll({

    attributes: { 

      exclude: ["content", 'updatedAt', 'deletedAt'] 

    },

    order: [

      ['createdAt', 'DESC']

    ]

  })

  .then((result) => {

    let resp = {

      meta: {

        total: result.length,

        url: '/posts/'

      },

      data: result

    }

    res.status(200).json(resp)
    
  })

  .catch((e) => {console.log(e);})
    
  },

  detail: (req, res, next) => {

    db.Blog.findOne({

      attributes: { 

        exclude: ['updatedAt', 'deletedAt'] 

      },

      where: {

        id: req.params.id

      }

    })

    .then((result) => {

      if (!result) {

        let resp = {

          meta: {

            message: 'Post Not Found'
    
          },
    
          data: result
    
        }

        res.status(404).json(resp)

      } else {

        let resp = {

          meta: {
    
            url: '/posts/' + result.id
    
          },
    
          data: result
    
        }
    
        res.status(200).json(resp)
      }

    })

    .catch((e) => {console.log(e);})
    
  },

  create: (req, res, next) => {

    let errors = validationResult(req)

    if (errors.isEmpty()) {

      db.Blog.create({

        title: req.body.title,

        content:req.body.content,

        image: 'http://localhost:3000/images/' + req.files[0].filename,

        category: req.body.category,

      })

      .then((result) => {

        let resp = {

          meta: {
    
            message: 'Post Created',
    
            url: '/posts/' + result.id
    
          },
    
          data: result
    
        }
    
        res.status(200).json(resp)

      })

      .catch((e) => {console.log(e);})

    } else {

      let message = errors.array()

      let errorsArray = []
      
      message.forEach((el) => {
       
        errorsArray.push(el.msg)  

      })

      res.status(404).json(errorsArray);
    
    }

  },

  update: (req, res, next) => {

    let errors = validationResult(req)

    if (errors.isEmpty()) {

      db.Blog.update({

        title: req.body.title,

        content:req.body.content,

        category: req.body.category,

      }, {

        where: {

          id: req.params.id

        }

      })

      .then((result) => {

        if (result == 0) {

          let resp = {

            meta: {
  
              message: 'Post Not Found'
      
            },
      
            data: result
      
          }
  
          res.status(404).json(resp)

        } else {

          let resp = {

            meta: {
  
              message: 'Post Updated'
      
            },
      
            data: result
      
          }
  
          res.status(200).json(resp)

        }

      })

      .catch((e) => {console.log(e);})

    } else {

      let message = errors.array()

      let errorsArray = []
      
      message.forEach((el) => {
       
        errorsArray.push(el.msg)  

      })

      res.status(404).json(errorsArray);
    
    }

  },

  destroy: (req, res, next) => {

    db.Blog.destroy({

      where: {

        id: req.params.id

      }

    })

    .then((result) => {


      if (result == 0) {

        let resp = {

          meta: {

            message: 'Post Not Found'
    
          },
    
          data: result
    
        }

        res.status(404).json(resp)

      } else {

        let resp = {

          meta: {

            message: 'Post Deleted'
    
          },
    
          data: result
    
        }

        res.status(200).json(resp)

      }

    })

    .catch((e) => {console.log(e);})

  }

}